# phpPassphrase

Very basic passphrase generator for low security use such as guest wifi. Inspired by the passwd.me API.
Includes a copy of the EFF wordlist from https://www.eff.org/deeplinks/2016/07/new-wordlists-random-passphrases

**Note** WPA2 PSK (pre-shared key) is 32 bytes (256 bits). According to the
         802.11i specification, a pass-phrase is a sequence of between
         8 and 63 ASCII-encoded characters. This code takes a length in words,
         not in characters. A script using this API needs to validate the length.

# Examples
## Bash
`curl "https://website.example/get_password.php?length=4&wordset=words"`
## Powershell
`$pass = (Invoke-WebRequest -Uri "https://website.example/get_password.php?length=4&wordset=words").Content`
## Python
```
import requests
url = "https://website.example/get_password.php"
params = {'length': 4, 'wordset': 'words'}
response = requests.get(url, params=params)
if response.status_code == 200:
    password = json.loads(response.content.decode('utf-8'))
else:
    print('Error')
print(response)
```
## Ruby
```
require 'net/http'
require 'json'

url = 'https://website.example/get_password.php?length=4&wordset=words'
uri = URI(url)
pass = Net::HTTP.get(uri)
```