<?php
/* basic API interface for requesting a passphrase */

/* please use this under PHP 7.1.0 or later (Mersenne Twister Random Number Generator vs the  libc rand function) /*

function badrequest() { // bad request to the API
    $httpStatusCode = 400;
    $httpStatusMsg  = 'Bad Request';
    $phpSapiName    = substr(php_sapi_name(), 0, 3);
    if ($phpSapiName == 'cgi' || $phpSapiName == 'fpm') {
        header('Status: '.$httpStatusCode.' '.$httpStatusMsg);
    } else {
        $protocol = isset($_SERVER['SERVER_PROTOCOL']) ? $_SERVER['SERVER_PROTOCOL'] : 'HTTP/1.0';
        header($protocol.' '.$httpStatusCode.' '.$httpStatusMsg);
    }
    exit();
}

function servererror() { // a missing file or other error on the server side
    $httpStatusCode = 500;
    $httpStatusMsg  = 'Internal Server Error';
    $phpSapiName    = substr(php_sapi_name(), 0, 3);
    if ($phpSapiName == 'cgi' || $phpSapiName == 'fpm') {
        header('Status: '.$httpStatusCode.' '.$httpStatusMsg);
    } else {
        $protocol = isset($_SERVER['SERVER_PROTOCOL']) ? $_SERVER['SERVER_PROTOCOL'] : 'HTTP/1.0';
        header($protocol.' '.$httpStatusCode.' '.$httpStatusMsg);
    }
    exit();
}

/* lots of sanity checks, limitations */
$wordsets = array("words", "wordsshort", "fruits", "words-bip39"); // hard coded in this example to limit abuse

$length = $wordset = '';
if ($_SERVER['REQUEST_METHOD'] == 'GET') {
  $length = trim(stripslashes(htmlspecialchars($_GET['length']))); // number of words
  $wordset = trim(stripslashes(htmlspecialchars($_GET['wordset']))); // name of wordset
  unset($_GET['length']);
  unset($_GET['wordset']);
} else { badrequest(); }
if ($_GET) { // unwanted parameters
    foreach ($_GET as &$value) {
        badrequest();
        die();
    }
}
if ($length && $wordset) { // both parameters
    $validated = 1;
} else { badrequest(); }
if (!in_array($wordset, $wordsets)) { // is it an expected wordlist
    badrequest();
}
if (strval($length) !== strval(intval($length)) || intval($length) < 1 || intval($length) > 10 ) { // length 1-10
    badrequest();
}
if (strlen($wordset) > 11) { // wordset file name too long
    badrequest();
}
if (!file_exists("$wordset.txt") || filesize("$wordset.txt") > ( 1 * 1024 * 1024)) { // file missing or too big
    servererror();
}
$lines = file("$wordset.txt", FILE_IGNORE_NEW_LINES);
if (count($lines) < $length || count($lines) > 65535) { // prevent short or long wordlist
    servererror();
}
shuffle($lines);
$passphrase = '';
for ($x = 0; $x < $length; $x++) {
    $passphrase .= array_pop($lines);
}
echo $passphrase;
?>
